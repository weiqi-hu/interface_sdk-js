COLLECT_APPLICATION_API

Usage
1.Install
  Install the npm dependencies(You must have node&npm installed):
  $npm install

2.Quick Start
  In the src directory
  $node format

3.Directory Structure
  ./sdk--- directory path of API files
  ./application--- directory path of application
  ./deps--- directory path of source code
  ./src--- directory path of result