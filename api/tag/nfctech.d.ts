/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import tag from '../@ohos.nfc.tag';
import { TagSession } from './tagSession';
import { AsyncCallback, Callback } from '../basic';

/**
 * Provides interfaces to control the read and write of tags that support the NFC-A technology.
 *
 * <p>This class is inherited from the {@link TagSession} abstract class, and provides methods to create
 * {@code NfcATag} objects and obtain the ATQA and SAK.
 *
 * @since 7
 * @syscap SystemCapability.Communication.NFC.Core
 */
export interface NfcATag extends TagSession {
  /**
   * Obtains the SAK of an NFC-A tag.
   *
   * @return Returns the SAK of the NFC-A tag.
   * @permission ohos.permission.NFC_TAG
   *
   * @since 7
   */
  getSak(): number;

  /**
   * Obtains the ATQA of an NFC-A tag.
   *
   * @return Returns the ATQA of the NFC-A tag.
   * @permission ohos.permission.NFC_TAG
   *
   * @since 7
   */
  getAtqa(): number[];
}

/**
 * Provides interfaces to create an {@code NfcBTag} and perform I/O operations on the tag.
 *
 * <p>This class inherits from the {@link TagSession} abstract class and provides interfaces to create an
 * {@code NfcBTag} and obtain the tag information.
 *
 * @since 7
 * @syscap SystemCapability.Communication.NFC.Core
 */
export interface NfcBTag extends TagSession {
  /**
   * Obtains the application data of a tag.
   *
   * @return Returns the application data of the tag.
   * @permission ohos.permission.NFC_TAG
   *
   * @since 7
   */
  getRespAppData(): number[];

  /**
   * Obtains the protocol information of a tag.
   *
   * @return Returns the protocol information of the tag.
   * @permission ohos.permission.NFC_TAG
   *
   * @since 7
   */
  getRespProtocol(): number[];
}

/**
 * Provides methods for creating an NFC-F tag, obtaining tag information, and controlling tag read and write.
 *
 * <p>This class inherits from the {@link TagSession} abstract class and provides interfaces to create an
 * {@code NfcFTag} and obtain the tag information.
 *
 * @since 7
 * @syscap SystemCapability.Communication.NFC.Core
 */
export interface NfcFTag extends TagSession {
  /**
   * Obtains the system code from this {@code NfcFTag} instance.
   *
   * @return Returns the system code.
   * @permission ohos.permission.NFC_TAG
   *
   * @since 7
   */
  getSystemCode(): number[];

  /**
   * Obtains the PMm (consisting of the IC code and manufacturer parameters) from this {@code NfcFTag} instance.
   *
   * @return Returns the PMm.
   * @permission ohos.permission.NFC_TAG
   *
   * @since 7
   */
  getPmm(): number[];
}

/**
 * Provides methods for creating an NFC-V tag, obtaining tag information, and controlling tag read and write.
 *
 * <p>This class inherits from the {@link TagSession} abstract class and provides interfaces to create an
 * {@code NfcVTag} and obtain the tag information.
 *
 * @since 7
 * @syscap SystemCapability.Communication.NFC.Core
 */
export interface NfcVTag extends TagSession {
  /**
   * Obtains the response flags from this {@code NfcVTag} instance.
   *
   * @return Returns the response flags.
   * @permission ohos.permission.NFC_TAG
   *
   * @since 7
   */
  getResponseFlags(): number;

  /**
   * Obtains the data storage format identifier (DSFID) from this {@code NfcVTag} instance.
   *
   * @return Returns the DSFID.
   * @permission ohos.permission.NFC_TAG
   *
   * @since 7
   */
  getDsfId(): number;
}

/**
 * Provides methods for accessing IsoDep tag.
 *
 * @since 9
 * @syscap SystemCapability.Communication.NFC.Core
 */
export interface IsoDepTag extends TagSession {
 /**
  * Gets IsoDep Historical bytes of the tag, which is based on NfcA RF technology.
  * It could be null if not based on NfcA.
  *
  * @return { number[] } Returns the Historical bytes, the length could be 0.
  * @since 9
  */
  getHistoricalBytes(): number[];

 /**
  * Gets IsoDep HiLayer Response bytes of the tag, which is based on NfcB RF technology.
  * It could be null if not based on NfcB.
  *
  * @return { number[] } Returns HiLayer Response bytes, the length could be 0.
  * @since 9
  */
  getHiLayerResponse(): number[];

 /**
  * Checks if extended apdu length supported or not.
  *
  * @return { boolean } Returns true if extended apdu length supported, otherwise false.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  isExtendedApduSupported(): Promise<boolean>;
  isExtendedApduSupported(callback: AsyncCallback<boolean>): void;
}

export interface NdefMessage {
 /**
  * Obtains all records of an NDEF message.
  *
  * @return { tag.NdefRecord[] } Records the list of NDEF records.
  * @since 9
  */
  getNdefRecords(): tag.NdefRecord[];

 /**
  * Creates an NDEF record with uri data.
  *
  * @param { string } uri - Uri data for new NDEF record.
  * @return { tag.NdefRecord } The instance of NdefRecord.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  makeUriRecord(uri: string): tag.NdefRecord;

 /**
  * Creates an NDEF record with text data.
  *
  * @param { string } text - Text data for new an NDEF record.
  * @param { string } locale - Language code for the NDEF record. if locale is null, use default locale.
  * @return { tag.NdefRecord } The instance of NdefRecord.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  makeTextRecord(text: string, locale: string): tag.NdefRecord;

 /**
  * Creates an NDEF record with mime data.
  *
  * @param { string } mimeType type of mime data for new an NDEF record.
  * @param { string } mimeData mime data for new an NDEF record.
  * @return { tag.NdefRecord } The instance of NdefRecord.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  makeMimeRecord(mimeType: string, mimeData: number[]): tag.NdefRecord;

 /**
  * Creates an NDEF record with external data.
  *
  * @param { string } domainName - Domain name of issuing organization for the external data.
  * @param { string } serviceName - Domain specific type of data for the external data.
  * @param { number[] } externalData - Data payload of an NDEF record.
  * @return { tag.NdefRecord } The instance of NdefRecord.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  makeExternalRecord(domainName: string, serviceName: string, externalData: number[]): tag.NdefRecord;

 /**
  * Parses an NDEF message into raw bytes.
  *
  * @param { NdefMessage } ndefMessage - An NDEF message to parse.
  * @return { number[] } Returns the raw bytes of an NDEF message.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  messageToBytes(ndefMessage: NdefMessage): number[];
}

/**
 * Provides methods for accessing NDEF tag.
 *
 * @since 9
 * @syscap SystemCapability.Communication.NFC.Core
 */
export interface NdefTag extends TagSession {
 /**
  * Creates an NDEF message with raw bytes.
  *
  * @param { number[] } data - The raw bytes to parse NDEF message.
  * @return { NdefMessage } The instance of NdefMessage.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  createNdefMessage(data: number[]): NdefMessage;

 /**
  * Creates an NDEF message with record list.
  *
  * @param { tag.NdefRecord[] } ndefRecords - The NDEF records to parse NDEF message.
  * @return { NdefMessage } The instance of NdefMessage.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  createNdefMessage(ndefRecords: tag.NdefRecord[]): NdefMessage;

 /**
  * Gets the type of NDEF tag.
  *
  * @return { tag.NfcForumType } The type of NDEF tag.
  * @since 9
  */
  getNdefTagType(): tag.NfcForumType;

 /**
  * Gets the NDEF message that was read from NDEF tag when tag discovery.
  *
  * @return { NdefMessage } The instance of NdefMessage.
  * @since 9
  */
  getNdefMessage(): NdefMessage;

 /**
  * Checks if NDEF tag is writable.
  *
  * @return { boolean } Returns true if the tag is writable, otherwise returns false.
  * @since 9
  */
  isNdefWritable(): boolean;

 /**
  * Reads NDEF message on this tag.
  *
  * @return { NdefMessage } The NDEF message in tag.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  readNdef(): Promise<NdefMessage>;
  readNdef(callback: AsyncCallback<NdefMessage>): void;

 /**
  * Writes NDEF message into this tag.
  *
  * @param { NdefMessage } msg - The NDEF message to be written.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  writeNdef(msg: NdefMessage): Promise<void>;
  writeNdef(msg: NdefMessage, callback: AsyncCallback<void>): void;

 /**
  * Checks NDEF tag can be set read-only.
  *
  * @return { boolean } Returns true if the tag can be set readonly, otherwise returns false.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  canSetReadOnly(): boolean;

 /**
  * Sets the NDEF tag read-only.
  *
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  setReadOnly(): Promise<void>;
  setReadOnly(callback: AsyncCallback<void>): void;

 /**
  * Converts the NFC forum type into string defined in NFC forum.
  *
  * @param { tag.NfcForumType } type - NFC forum type of NDEF tag.
  * @return { string } The NFC forum string type.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  getNdefTagTypeString(type: tag.NfcForumType): string;
}

/**
 * Provides methods for accessing MifareClassic tag.
 *
 * @since 9
 * @syscap SystemCapability.Communication.NFC.Core
 */
export interface MifareClassicTag extends TagSession {
 /**
  * Authenticates a sector with the key.Only successful authentication sector can be operated.
  *
  * @param { number } sectorIndex - Index of sector to authenticate.
  * @param { number[] } key - The key(6-bytes) to authenticate.
  * @param { boolean } isKeyA - KeyA flag. true means KeyA, otherwise KeyB.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  authenticateSector(sectorIndex: number, key: number[], isKeyA: boolean): Promise<void>;
  authenticateSector(sectorIndex: number, key: number[], isKeyA: boolean, callback: AsyncCallback<void>): void;

 /**
  * Reads a block, one block size is 16 bytes.
  *
  * @param { number } blockIndex - The index of block to read.
  * @return { number[] } Returns the block data.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  readSingleBlock(blockIndex: number): Promise<number[]>;
  readSingleBlock(blockIndex: number, callback: AsyncCallback<number[]>): void;

 /**
  * Writes a block, one block size is 16 bytes.
  *
  * @param { number } blockIndex - The index of block to write.
  * @param { number } data - The block data to write.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  writeSingleBlock(blockIndex: number, data: number[]): Promise<void>;
  writeSingleBlock(blockIndex: number, data: number[], callback: AsyncCallback<void>): void;

 /**
  * Increments the contents of a block, and stores the result in the internal transfer buffer.
  *
  * @param { number } blockIndex - The index of block to increment.
  * @param { number } value - The value to increment, none-negative.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  incrementBlock(blockIndex: number, value: number): Promise<void>;
  incrementBlock(blockIndex: number, value: number, callback: AsyncCallback<void>): void;

 /**
  * Decrements the contents of a block, and stores the result in the internal transfer buffer.
  *
  * @param { number } blockIndex - The index of block to decrement.
  * @param { number } value - The value to decrement, none-negative.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  decrementBlock(blockIndex: number, value: number): Promise<void>;
  decrementBlock(blockIndex: number, value: number, callback: AsyncCallback<void>): void;

 /**
  * Writes the contents of the internal transfer buffer to a value block.
  *
  * @param { number } blockIndex - The index of value block to be written.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  transferToBlock(blockIndex: number): Promise<void>;
  transferToBlock(blockIndex: number, callback: AsyncCallback<void>): void;

 /**
  * Moves the contents of a block into the internal transfer buffer.
  *
  * @param { number } blockIndex - The index of value block to be moved from.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  restoreFromBlock(blockIndex: number): Promise<void>;
  restoreFromBlock(blockIndex: number, callback: AsyncCallback<void>): void;

 /**
  * Gets the number of sectors in MifareClassic tag.
  *
  * @return { number } Returns the number of sectors.
  * @since 9
  */
  getSectorCount(): number;

 /**
  * Gets the number of blocks in the sector.
  *
  * @param { number } sectorIndex - The index of sector.
  * @return { number } Returns the number of blocks.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  getBlockCountInSector(sectorIndex: number): number;

 /**
  * Gets the type of the MifareClassic tag.
  *
  * @return { tag.MifareClassicType } Returns type of MifareClassic tag.
  * @since 9
  */
  getType(): tag.MifareClassicType;

 /**
  * Gets size of the tag in bytes.
  *
  * @return { number } Returns the size of the tag.
  * @since 9
  */
  getTagSize(): number;

 /**
  * Checks if the tag is emulated or not.
  *
  * @return { boolean } Returns true if tag is emulated, otherwise false.
  * @since 9
  */
  isEmulatedTag(): boolean;

 /**
  * Gets the first block of the specific sector.
  *
  * @param { number } sectorIndex - The index of sector.
  * @return { number } Returns index of first block in the sector.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  getBlockIndex(sectorIndex: number): number;

 /**
  * Gets the sector index, that the sector contains the specific block.
  *
  * @param { number } blockIndex - The index of block.
  * @return { number } Returns the sector index.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @since 9
  */
  getSectorIndex(blockIndex: number): number;
}

/**
 * Provides methods for accessing MifareUltralight tag.
 *
 * @since 9
 * @syscap SystemCapability.Communication.NFC.Core
 */
export interface MifareUltralightTag extends TagSession {
 /**
  * Reads 4 pages, total is 16 bytes. Page size is 4 bytes.
  *
  * @param { number } pageIndex - The index of page to read.
  * @return { number[] } Returns 4 pages data.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  readMultiplePages(pageIndex: number): Promise<number[]>;
  readMultiplePages(pageIndex: number, callback: AsyncCallback<number[]>): void;

 /**
  * Writes a page, total 4 bytes.
  *
  * @param { number } pageIndex - The index of page to write.
  * @param { number[] } data - The page data to write.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  writeSinglePage(pageIndex: number, data: number[]): Promise<void>;
  writeSinglePage(pageIndex: number, data: number[], callback: AsyncCallback<void>): void;

  /**
  * Gets the type of the MifareUltralight tag.
  *
  * @return { tag.MifareUltralightType } Returns the type of MifareUltralight tag.
  * @since 9
  */
  getType(): tag.MifareUltralightType;
}

/**
 * Provides methods for accessing NdefFormatable tag.
 *
 * @since 9
 * @syscap SystemCapability.Communication.NFC.Core
 */
export interface NdefFormatableTag extends TagSession {
 /**
  * Formats a tag as NDEF tag, writes NDEF message into the NDEF Tag.
  *
  * @param { NdefMessage } message - NDEF message to write while format. It can be null, then only format the tag.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  format(message: NdefMessage): Promise<void>;
  format(message: NdefMessage, callback: AsyncCallback<void>): void;

 /**
  * Formats a tag as NDEF tag, writes NDEF message into the NDEF Tag, then sets the tag readonly.
  *
  * @param { NdefMessage } message - NDEF message to write while format. It can be null, then only format the tag.
  * @permission ohos.permission.NFC_TAG
  * @throws { BusinessError } 201 - Permission denied.
  * @throws { BusinessError } 401 - The parameter check failed.
  * @throws { BusinessError } 3100201 - Tag running state of service is abnormal.
  * @since 9
  */
  formatReadOnly(message: NdefMessage): Promise<void>;
  formatReadOnly(message: NdefMessage, callback: AsyncCallback<void>): void;
}